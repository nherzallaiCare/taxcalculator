﻿using System;
using System.Threading.Tasks;
using TaxCalculator.Models;

namespace TaxCalculator.Repositories
{
    public interface ICalculateTaxStrategy
    {
        Task<taxresult> CalculateTax(TaxInfo taxInfo);
     
    }
}
