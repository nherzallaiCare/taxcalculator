﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TaxCalculator.Models;

namespace TaxCalculator.Repositories
{
    public class TaxForOrderCA : ICalculateTaxStrategy
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IOptions<Settings> _settings;

        public TaxForOrderCA(IHttpClientFactory clientFactory, IOptions<Settings> settings)
        {
            _clientFactory = clientFactory;
            _settings = settings;
        }
        public async Task<taxresult> CalculateTax(TaxInfo taxInfo)
        {
            var token = "\"" + _settings.Value.APICode + "\"";
            var json = JsonSerializer.Serialize(taxInfo);

            var request = new HttpRequestMessage(HttpMethod.Post,
              _settings.Value.TaxBaseURL + "/taxes");

            request.Content = new StringContent(json,
                                   Encoding.UTF8,
                                   "application/json");

            request.Headers.Add("Authorization", "Token token=" + token);

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                var tax = JsonSerializer.Deserialize<taxresult>(content);
                return tax;

            }
            return new taxresult();

        }

    }
}
