﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TaxCalculato.Models;

namespace TaxCalculator.Repositories
{
    public class TaxRepository: ITaxRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IOptions<Settings> _settings;

        public TaxRepository(IHttpClientFactory clientFactory, IOptions<Settings> settings)
        {
            _clientFactory = clientFactory;
            _settings = settings;
        }

        public async Task<RateObj> GetRate(RateQuery rateQuery)
        {
            var token = "\"" + _settings.Value.APICode + "\"";

            var request = new HttpRequestMessage(HttpMethod.Get,
              _settings.Value.TaxBaseURL + "/rates" + "/" + rateQuery.ZipCode + "?" + "country=" + rateQuery.Country + "&city=" + rateQuery.City);
            request.Headers.Add("Authorization", "Token token=" + token);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var rate =  JsonSerializer.Deserialize<RateObj>(content);
                return rate;
               
            }
            return null;
            
        }
    }
}
