﻿using System;
using System.Collections;
using System.Threading.Tasks;
using TaxCalculato.Models;

namespace TaxCalculator.Repositories
{
    public interface ITaxRepository
    {
        Task<RateObj> GetRate(RateQuery rateQuery);
        
    }
}
