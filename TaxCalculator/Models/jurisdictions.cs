﻿using System;
namespace TaxCalculator.Models
{
    public class jurisdictions
    {
        public jurisdictions()
        {
        }
        public string city { get; set; }
        public string country { get; set; }
        public string county { get; set; }
        public string state { get; set; }
    }
}
