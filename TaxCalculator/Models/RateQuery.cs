﻿using System;
namespace TaxCalculato.Models
{
    public class RateQuery
    {
        public RateQuery()
        {
        }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
