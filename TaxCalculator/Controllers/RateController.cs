﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaxCalculato.Models;
using TaxCalculator.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaxCalculator.Controllers
{
    [Produces("application/json")]
    [Route("api/rate")]
    public class RateController : Controller
    {
        ITaxRepository _taxRepository;
        public RateController(ITaxRepository taxRepository)
        {
            _taxRepository = taxRepository;
        }
      
        [HttpGet("{zipcode}")]
        public async Task<IActionResult> Get(string zipcode,string country,string city)
        {
            var rateQuery = new RateQuery
            {
                ZipCode = zipcode,
                Country = country,
                City = city
            };
            var rates = await _taxRepository.GetRate(rateQuery);

            return Ok(rates);
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
