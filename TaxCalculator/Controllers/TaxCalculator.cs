﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TaxCalculator.Models;
using TaxCalculator.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaxCalculator.Controllers
{
    [Route("api/[controller]")]
    public class TaxCalculator : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IOptions<Settings> _settings;
        private ICalculateTaxStrategy _calculateTaxStrategy;

        public TaxCalculator(ICalculateTaxStrategy calculateTaxStrategy, IHttpClientFactory clientFactory, IOptions<Settings> settings)
        {
            _calculateTaxStrategy = calculateTaxStrategy;
            _clientFactory = clientFactory;
            _settings = settings;
        }
        // POST api/values
        [HttpPost("{type}")]
        public async Task<IActionResult> PostAsync(string type,[FromBody]TaxInfo taxInfo )
        {
            switch (type)
            {
                case "NY":
                    _calculateTaxStrategy = new TaxForOrderNY(_clientFactory,_settings);
                    break;
                case "CA":
                    _calculateTaxStrategy = new TaxForOrderCA(_clientFactory, _settings);
                    break;
                case "Nexus":
                    _calculateTaxStrategy = new TaxForOrderNexus(_clientFactory, _settings);
                    break;
                case "Order":
                    _calculateTaxStrategy = new TaxForOrder(_clientFactory, _settings);
                    break;
                default:
                    _calculateTaxStrategy = new TaxForOrder(_clientFactory, _settings);
                    break;
            };


            var result = await _calculateTaxStrategy.CalculateTax(taxInfo);
            return Ok(result);

        }

       
    }
}
