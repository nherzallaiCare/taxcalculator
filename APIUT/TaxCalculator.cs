using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using TaxCalculator;
using TaxCalculator.Models;
using TaxCalculator.Repositories;

namespace APIUT
{
   
    public class Tests
    {

        private readonly IHttpClientFactory _clientFactory;
        private readonly IOptions<Settings> _settings;
        private ICalculateTaxStrategy _calculateTaxStrategy;

        public Tests(IHttpClientFactory clientFactory, IOptions<Settings> settings)
        {
            _clientFactory = clientFactory;
            _settings = settings;
        }
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CalculateOrderTax()
        {
            var line_item = new LineItems
            {
                 product_tax_code= "31000",
                 quantity =1,
                 unit_price=15
            };
            var line_items = new List<LineItems>();
            line_items.Add(line_item);

            var taxInfo = new TaxInfo
            {
                 amount = 16,
                 from_country = "US",
                 from_zip = "07001",
                 from_state = "NJ",
                 to_country = "US",
                 to_zip = "07446",
                 to_state="NJ",
                 shipping=1,
                 line_items = line_items

            };
            _calculateTaxStrategy = new TaxForOrder(_clientFactory, _settings);
            var tax = _calculateTaxStrategy.CalculateTax(taxInfo);
            Assert.AreEqual(tax.Result.tax.taxable_amount, 16);
            Assert.Pass();
        }
        [Test]
        public void CalculateOrderTaxNY()
        {
            var line_item = new LineItems
            {
                product_tax_code = "31000",
                quantity = 1,
                unit_price = 15
            };
            var line_items = new List<LineItems>();
            line_items.Add(line_item);

            var taxInfo = new TaxInfo
            {

                to_city = "Mahopac",
                to_state = "NY",
                to_zip =  "10541",
                to_country= "US",
                from_state= "NY",
                from_zip =  "12054",
                from_country= "US",
                amount = 29,
                shipping= 7,
                line_items = line_items

            };
            _calculateTaxStrategy = new TaxForOrderNY(_clientFactory, _settings);
            var tax = _calculateTaxStrategy.CalculateTax(taxInfo);
            Assert.AreEqual(tax.Result.tax.shipping, 7);
            Assert.Pass();
        }
    }
}